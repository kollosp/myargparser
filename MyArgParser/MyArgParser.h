#ifndef MYARGPARSER_H
#define MYARGPARSER_H

#include <string>
#include <vector>

class MyArgParser
{
public:
    struct Field{
        Field(const std::string& m_iName, const std::string& m_iValue):
            name(m_iName), value(m_iValue){}

        std::string name;
        std::string value;
    };

public:
    MyArgParser();

    void parse(int argc, char **argv);

    bool hasOption(const std::string& m_name) const noexcept;
    bool hasValue(const std::string& m_name) const noexcept;
    std::string& value(const std::string& m_name);

    const std::string& value(const std::string& m_name) const;

private:

    std::vector<std::string> c_options; //wszytkie wolne slowa nierozpoczynajace sie od '-'
    std::vector<Field> c_params; //nazwa parametru zaczyna sie od '-' a po nazwie nastepuje wartosc

    friend std::ostream &operator <<(std::ostream& m_iStr, const MyArgParser& m_iParser);
    friend std::string ParseJSON(const MyArgParser& m_iParser) noexcept;
};

std::string ParseJSON(const MyArgParser& m_iParser) noexcept;

#endif // MYARGPARSER_H
