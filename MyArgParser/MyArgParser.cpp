#include "MyArgParser.h"

#include <iostream>
#include <algorithm>

MyArgParser::MyArgParser()
{

}

void MyArgParser::parse(int argc, char **argv)
{
    for(int i=0;i<argc;++i){

        if(argv[i][0] == '-' && i!=argc-1){

            if(argv[i+1][0]=='-'){
                c_options.push_back(argv[i]);
            }
            else
            {
                c_params.push_back(Field(argv[i], argv[i+1]));
                ++i;
            }
        }
        else {
            c_options.push_back(argv[i]);
        }
    }
}

bool MyArgParser::hasOption(const std::string &m_name) const noexcept
{
    return std::find(c_options.begin(), c_options.end(), m_name) != c_options.end();
}

bool MyArgParser::hasValue(const std::string &m_name) const noexcept
{
    return std::find_if(c_params.begin(), c_params.end(),
                        [&](const Field&f) -> bool {return f.name == m_name;}) != c_params.end();
}

std::string &MyArgParser::value(const std::string &m_name)
{
    auto it = std::find_if(c_params.begin(), c_params.end(),
                        [&](const Field&f) -> bool {return f.name == m_name;});

    if(it == c_params.end())
        throw (std::out_of_range("std::string &MyArgParser::value"));

    return it->value;
}

const std::string &MyArgParser::value(const std::string &m_name) const
{
    auto it =std::find_if(c_params.begin(), c_params.end(),
                        [&](const Field&f) -> bool {return f.name == m_name;});

    if(it == c_params.end())
        throw (std::out_of_range("std::string &MyArgParser::value"));

    return it->value;
}

std::string ParseJSON(const MyArgParser &m_iParser) noexcept
{
    std::string m_ret = "arguments:{options:[";

    for(unsigned int i=0;i<m_iParser.c_options.size();++i){
        m_ret+=m_iParser.c_options[i];

        if(i!=m_iParser.c_options.size()-1)
            m_ret += ",";
    }
    m_ret+="],params:[";


    for(unsigned int i=0;i<m_iParser.c_params.size();++i){
        m_ret+="{"+m_iParser.c_params[i].name+":"+m_iParser.c_params[i].value+"}";

        if(i!=m_iParser.c_params.size()-1)
            m_ret+=",";
    }

    m_ret+="]}";
    return m_ret;
}

std::ostream &operator <<(std::ostream &m_iStr, const MyArgParser &m_iParser)
{
    m_iStr<<"arguments:{options:[";

    for(unsigned int i=0;i<m_iParser.c_options.size();++i){
        m_iStr<<m_iParser.c_options[i];

        if(i!=m_iParser.c_options.size()-1)
            m_iStr<<",";
    }
    m_iStr<<"],params:[";


    for(unsigned int i=0;i<m_iParser.c_params.size();++i){
        m_iStr<<"{"<<m_iParser.c_params[i].name<<":"<<m_iParser.c_params[i].value<<"}";

        if(i!=m_iParser.c_params.size()-1)
            m_iStr<<",";
    }

    m_iStr<<"]}";
    return m_iStr;
}
